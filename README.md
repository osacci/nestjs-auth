## Description

Authentication REST API is built with:

- [Nest](https://github.com/nestjs/nest) framework for implementing REST API
- [PostgreSQL](https://www.postgresql.org/) object-relational database system
- [Prisma](https://www.prisma.io/docs/concepts/overview/what-is-prisma) ORM for managing dabase migrations and connecting REST API code to the dabase

## Prerequisites

Local development requires PostgreSQL database running with credentials specified in [./.env](./.env).
The easiest way to run it is with [docker-compose](https://docs.docker.com/compose/).

```bash
$ docker-compose up -d
```

**Note:** The above command also builds and starts REST API app at port `80`. See [Running with docker-compose](#running-with-docker-compose)

## Installation

```bash
$ npm install
```

## Database migrations

```bash
$ npm run prisma:migrate:deploy
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running with docker-compose

You can build and start the app with [docker-compose](https://docs.docker.com/compose/).

```bash
$ docker-compose up -d
```

After running the above command, the app is available at port `80`.

Currently the app running inside docker-compose connects to the same database as the app in the dev mode.
Connection credentials are specified in `.env.docker`.

**Note:** Environment variables provided in `.env.docker` are provided there for demonstration purposes only.
When deploying to production, proper secret management tools should be used.

## API docs

After starting the app, Swagger docs are available at `/docs`.

Postman collection is available at [./postman_collection.json](./postman_collection.json).
