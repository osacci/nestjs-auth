import { Controller, Get } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ACCESS_TOKEN_BEARER_NAME } from 'src/auth/auth.constants';
import { TokenPayload } from 'src/auth/auth.types';
import { GetTokenPayload } from 'src/shared/decorators';
import { DataDto } from './data.dto';

@ApiTags('data')
@Controller('data')
export class DataController {
  @ApiBearerAuth(ACCESS_TOKEN_BEARER_NAME)
  @ApiOperation({
    summary:
      'Demonstrates protected access with short-lived jwt token (access token)',
  })
  @ApiOkResponse({
    type: DataDto,
  })
  @Get('protected')
  protectedData(@GetTokenPayload() tokenPayload: TokenPayload): DataDto {
    return {
      message: `sub ${tokenPayload.sub}, email: ${tokenPayload.email} can see this protected message`,
    };
  }
}
