import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DataDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  message: string;
}
