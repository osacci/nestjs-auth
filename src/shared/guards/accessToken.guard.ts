import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { ACCESS_TOKEN_STRATEGY_NAME } from 'src/auth/strategies';
import { SKIP_ACCESS_TOKEN_GUARD_FLAG } from '../decorators';

@Injectable()
export class AccessTokenGuard extends AuthGuard(ACCESS_TOKEN_STRATEGY_NAME) {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const skipGuard = this.reflector.getAllAndOverride(
      SKIP_ACCESS_TOKEN_GUARD_FLAG,
      [context.getHandler(), context.getClass()],
    );

    if (skipGuard) {
      return true;
    }

    return super.canActivate(context);
  }
}
