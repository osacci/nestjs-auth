export interface TokenPayload {
  email: string;
  sub: string;
  tokenVersionId: string;
}
