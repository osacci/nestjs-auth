import {
  UnauthorizedException,
  Injectable,
  BadRequestException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from '../prisma/prisma.service';

import { AccessTokenDto, AuthDto, TokensDto } from './auth.dto';
import { TokenPayload } from './auth.types';

import * as bcrypt from 'bcrypt';
import { User } from '@prisma/client';
import { nanoid } from 'nanoid';

const BCRYPT_HASH_ROUNDS = 10;

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private config: ConfigService,
  ) {}

  async register(dto: AuthDto): Promise<User> {
    const passwordHash = await bcrypt.hash(dto.password, BCRYPT_HASH_ROUNDS);
    const user = await this.prisma.user
      .create({
        data: {
          email: dto.email,
          passwordHash,
          tokenVersionId: nanoid(),
        },
      })
      .catch((error) => {
        if (error instanceof PrismaClientKnownRequestError) {
          if (error.code === 'P2002') {
            throw new BadRequestException(
              'User with this email already exists',
            );
          }
        }
        throw error;
      });

    return user;
  }

  async login(dto: AuthDto): Promise<TokensDto> {
    const UNAUTHORIZED_MESSAGE = 'Incorrect email or password';
    const user = await this.prisma.user.findUnique({
      where: {
        email: dto.email,
      },
    });

    if (!user) {
      throw new UnauthorizedException(UNAUTHORIZED_MESSAGE);
    }

    const passwordMatches = await bcrypt.compare(
      dto.password,
      user.passwordHash,
    );
    if (!passwordMatches) {
      throw new UnauthorizedException(UNAUTHORIZED_MESSAGE);
    }

    const refreshToken = await this.generateRefreshToken(user);
    const accessToken = await this.generateAccessToken(user);

    return {
      refreshToken,
      accessToken,
    };
  }

  async logout(userId: string): Promise<void> {
    await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        tokenVersionId: nanoid(),
      },
    });
  }

  async generateAccessTokenForUserId(
    userId: string,
    tokenVersionId: string,
  ): Promise<Pick<AccessTokenDto, 'accessToken'>> {
    const user = await this.prisma.user.findUnique({
      where: {
        id: userId,
      },
    });

    if (!user || user.tokenVersionId !== tokenVersionId) {
      throw new UnauthorizedException();
    }

    const accessToken = await this.generateAccessToken(user);

    return {
      accessToken,
    };
  }

  async generateRefreshToken(user: User): Promise<string> {
    const jwtPayload: TokenPayload = {
      sub: user.id,
      email: user.email,
      tokenVersionId: user.tokenVersionId,
    };

    return this.jwtService.signAsync(jwtPayload, {
      secret: this.config.get<string>('REFRESH_TOKEN_SECRET'),
      expiresIn: '30d',
    });
  }

  async generateAccessToken(user: User): Promise<string> {
    const jwtPayload: TokenPayload = {
      sub: user.id,
      email: user.email,
      tokenVersionId: user.tokenVersionId,
    };

    return this.jwtService.signAsync(jwtPayload, {
      secret: this.config.get<string>('ACCESS_TOKEN_SECRET'),
      expiresIn: '2m',
    });
  }
}
