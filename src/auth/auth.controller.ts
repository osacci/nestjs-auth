import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';

import { SkipAccessTokenGuard, GetTokenPayload } from '../shared/decorators';
import { RefreshTokenGuard } from '../shared/guards';
import { AuthService } from './auth.service';
import { AccessTokenDto, AuthDto, RegisterDto, TokensDto } from './auth.dto';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { REFRESH_TOKEN_BEARER_NAME } from './auth.constants';
import { TokenPayload } from './auth.types';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({
    summary: 'Registers user with email and password',
  })
  @SkipAccessTokenGuard()
  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() dto: RegisterDto): Promise<void> {
    await this.authService.register(dto);
  }

  @ApiOperation({
    summary: 'Issues long-lived refresh token and short-lived access token',
  })
  @ApiOkResponse({
    type: TokensDto,
  })
  @SkipAccessTokenGuard()
  @Post('login')
  @HttpCode(HttpStatus.OK)
  login(@Body() dto: AuthDto): Promise<TokensDto> {
    return this.authService.login(dto);
  }

  @ApiOperation({
    summary:
      'Invalidates all refresh tokens issued to the current user. Requires refresh token.',
  })
  @ApiOkResponse()
  @ApiBearerAuth(REFRESH_TOKEN_BEARER_NAME)
  @SkipAccessTokenGuard()
  @UseGuards(RefreshTokenGuard)
  @Post('logout')
  @HttpCode(HttpStatus.OK)
  logout(@GetTokenPayload() tokenPayload: TokenPayload): Promise<void> {
    return this.authService.logout(tokenPayload.sub);
  }

  @ApiOperation({ summary: 'Issues short-lived access token' })
  @ApiOkResponse({
    status: HttpStatus.OK,
    type: AccessTokenDto,
  })
  @ApiBearerAuth(REFRESH_TOKEN_BEARER_NAME)
  @SkipAccessTokenGuard()
  @UseGuards(RefreshTokenGuard)
  @Post('getAccessToken')
  @HttpCode(HttpStatus.OK)
  getAccessToken(
    @GetTokenPayload() tokenPayload: TokenPayload,
  ): Promise<Pick<AccessTokenDto, 'accessToken'>> {
    return this.authService.generateAccessTokenForUserId(
      tokenPayload.sub,
      tokenPayload.tokenVersionId,
    );
  }
}
