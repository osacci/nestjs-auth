import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
  MinLength,
  Matches,
} from 'class-validator';

export class AuthDto {
  @ApiProperty({ format: 'email' })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({ format: 'password' })
  @IsNotEmpty()
  @IsString()
  password: string;
}

const PASSWORD_MATCH_MSG =
  'password must contain at least 1 upper case letter, 1 lower case letter, and at least 1 number or special character';
const PASSWORD_MIN_LENGTH = 8;

export class RegisterDto {
  @ApiProperty({ format: 'email' })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    minLength: PASSWORD_MIN_LENGTH,
    description: PASSWORD_MATCH_MSG,
    format: 'password',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: PASSWORD_MATCH_MSG,
  })
  password: string;
}

export class TokensDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  refreshToken: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  accessToken: string;
}

export class AccessTokenDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  accessToken: string;
}
